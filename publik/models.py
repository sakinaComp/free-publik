# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class reg_users(models.Model):
	
	username = models.EmailField(max_length=200)
	email = models.EmailField(max_length=200)
	password = models.CharField(max_length=100)
	country = models.CharField(max_length=100)
	countrycode = models.CharField(max_length=100)
	phone = models.CharField(max_length=100)
	company = models.CharField(max_length=100)
	companytype = models.CharField(max_length=100)
	industry = models.CharField(max_length=100)
	planid = models.CharField(max_length=100)
	# years = models.CharField(max_length=100)
	# months = models.CharField(max_length=100)
	contact_person = models.CharField(max_length=200)
	contact_person_designation = models.CharField(max_length=200)
	companyprofileurl = models.CharField(max_length=1000)
	profile_created_on = models.DateTimeField(auto_now=True)
	contact_person_phone = models.CharField(max_length=200)
	contact_person_email = models.CharField(max_length=200)
	category = models.CharField(max_length=100)
	profile = models.CharField(max_length=1000,null=True,blank=True)
	# budget = models.CharField(max_length=100)
	# current_agency = models.CharField(max_length=100)
	# social = models.CharField(max_length=100)
	# preference = models.CharField(max_length=100)
	# premium = models.CharField(max_length=100)
	# about = models.TextField(max_length=1000)
	# profile = models.CharField(max_length=100)
	def __str__(self):
		return self.email


# options = (('india','India'),('usa','USA'),('australia','Australia'))

class reg_freelancer(models.Model):
	
	# name = models.CharField(max_length=100)
	email = models.EmailField(max_length=200)
	password = models.CharField(max_length=100)
	username = models.CharField(max_length=100,primary_key=True)
	country = models.CharField(max_length=100)
	countrycode = models.CharField(max_length=100)
	phone = models.CharField(max_length=100)
	freelancertype = models.CharField(max_length=100)
	designation = models.CharField(max_length=100)
	company = models.CharField(max_length=100)
	expertise = models.CharField(max_length=500)
	years = models.CharField(max_length=100)
	months = models.CharField(max_length=100)
	# from_time = models.CharField(max_length=10)
	# to_time = models.CharField(max_length=10)	
	work1 = models.CharField(max_length=100,null=True,blank=True)
	work2 = models.CharField(max_length=100,null=True,blank=True)
	work3 = models.CharField(max_length=100,null=True,blank=True)
	work4 = models.CharField(max_length=100,null=True,blank=True)
	work5 = models.CharField(max_length=100,null=True,blank=True)
	work6 = models.CharField(max_length=100,null=True,blank=True)
	resume = models.CharField(max_length=100)
	profile = models.CharField(max_length=100,null=True,blank=True)
	rating = models.CharField(max_length=20)
	
	planid = models.CharField(max_length=5)

	def __str__(self):
		return self.username


class post(models.Model):
	job_type = models.CharField(max_length=100)
	brief = models.TextField(max_length=1000)
	budget = models.CharField(max_length=50)
	years = models.CharField(max_length=20)
	days = models.CharField(max_length=20)
	months = models.CharField(max_length=20)
	# team_requirement = models.CharField(max_length=10)
	deliverables = models.TextField(max_length=1000)
	author = models.CharField(max_length=50)
	date = models.DateTimeField(auto_now=True)
	applied = models.ManyToManyField(reg_freelancer, related_name="posts")
	selected = models.CharField(max_length=100)									# { 0 - applied | 1 - selected | 2 - Approved & finalized for project }
	freelancer_appointed = models.CharField(max_length=100)
	title = models.CharField(max_length=500)
	currency_type = models.CharField(max_length=20)
	status = models.CharField(max_length=20,default="open")
	deletedbyclient = models.BooleanField(default=False)
	deletedbyfreelancer = models.BooleanField(default=False)
	# apply_list = models.TextField(null=True)
	# number_of_applicant = models.IntegerField(default=0)
	def __str__(self):
		return self.job_type


class freelancer_notification(models.Model):
	candidate_name = models.CharField(max_length=100)
	post_id = models.CharField(max_length=10)
	create_at = models.DateTimeField(auto_now=True)
	Selected = models.CharField(max_length=100)

	def __str__(self):
		return self.candidate_name

