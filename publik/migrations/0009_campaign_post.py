# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-05-09 11:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publik', '0008_auto_20190508_1647'),
    ]

    operations = [
        migrations.CreateModel(
            name='campaign_post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('campaign_type', models.CharField(max_length=20)),
                ('brief', models.TextField(max_length=1000)),
                ('budget', models.CharField(max_length=50)),
                ('team_requirement', models.CharField(max_length=10)),
                ('milestones', models.TextField(max_length=1000)),
                ('author', models.CharField(max_length=50)),
                ('date', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
