# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-05-15 13:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publik', '0017_auto_20190515_1645'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='applied',
        ),
        migrations.AddField(
            model_name='post',
            name='apply_list',
            field=models.TextField(null=True),
        ),
    ]
