# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-06-14 10:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publik', '0025_auto_20190614_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='reg_users',
            name='username',
            field=models.CharField(default=None, max_length=100),
            preserve_default=False,
        ),
    ]
