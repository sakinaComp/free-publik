# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from models import reg_users, reg_freelancer, post
from django.core.files.storage import FileSystemStorage
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import connection
import json
from django.http import JsonResponse
from django.core.mail import send_mail, send_mass_mail
from django.template.loader import render_to_string
from datetime import datetime
from django.conf import settings
from random import randint
from django.contrib import messages
from django.core.mail import EmailMessage

# Create your views here.


# This is called once to count total number of users on website. Using this value in all the other functions.
def totalcount():	
	# reg_users_count= reg_users.objects.all().count()
	# reg_freelancer_count= reg_freelancer.objects.all().count()
	# total_count = reg_users_count + reg_freelancer_count
	total_count = 0	
	return total_count




def login(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		uname = request.session['uname']
		return redirect('/udashboard/')
	else:
		return render(request,'login.html',{'total_count':total_count})
	





def registration_user(request):
	total_count = totalcount()
	if request.method == 'POST':
		# name = request.POST.get('name')
		# username = request.POST.get('username')
		email = request.POST.get('email')
		pass_1 = request.POST.get('password1')
		pass_2 = request.POST.get('password2')
		country = request.POST.get('country')
		# countrycode = request.POST.get('countrycode')
		phone = request.POST.get('phone')
		company = request.POST.get('company')
		companytype = request.POST.get('companytype')
		industry = request.POST.get('industry')
		planid = request.POST.get('planid')
		# years = request.POST.get('years')
		# months = request.POST.get('months')
		contact_person = request.POST.get('contact_person')
		contact_person_designation = request.POST.get('contact_person_designation')
		company_profile = request.FILES['company_profile']

		if pass_1 == pass_2:
			user_details = reg_users.objects.all()
			if user_details:
				for each in user_details:
					if each.email == email:
						error = " EMAIL ALREADY EXISTS PLEASE LOGIN "
						return render(request, 'login.html',{"error":error,"total_count":total_count})
				else:
					for each in user_details:						
						if each.phone == phone:
							error = " PHONE NUMBER IS ALREADY USED "
							return render(request, 'login.html',{"error":error,"total_count":total_count})
					else:
						# request.session['urname'] = name
						# request.session['urusername'] = username
						request.session['uremail'] = email
						request.session['urpass_1'] = pass_1
						request.session['urcountry'] = country
						# request.session['urcountrycode'] = countrycode
						request.session['urphone'] = phone
						request.session['urcompany'] = company
						request.session['urcompanytype'] = companytype
						request.session['urindustry'] = industry
						request.session['urplanid'] = planid
						# request.session['uryears'] = years
						# request.session['urmonths'] = months						
						request.session['urcontact_person'] = contact_person
						request.session['urcontact_person_designation'] = contact_person_designation
						
						fs = FileSystemStorage()				
						url1 = fs.save(company_profile.name, company_profile)
						company_profile_url = fs.url(url1)
						request.session['urcompany_profile_url'] = company_profile_url

						otp = (randint(1000, 9999))
						otp = str(otp)
						request.session['val_otp1'] = otp
						subject = 'Freepublik Email Verification'
						msg_plain2 = render_to_string('clientotpemail.html', {})
						msg_html = render_to_string('clientotpemail.html', {'otp': otp})
						email_from = settings.SENDER_EMAIL
						recipient_list = [email]
						send_mail( subject, msg_plain2, email_from, recipient_list,html_message=msg_html )
						return render(request,'uotpvalidation.html',{'total_count':total_count})
						
						
						
						
						
					
			else:
				request.session['uremail'] = email
				request.session['urpass_1'] = pass_1
				request.session['urcountry'] = country
				# request.session['urcountrycode'] = countrycode
				request.session['urphone'] = phone
				request.session['urcompany'] = company
				request.session['urcompanytype'] = companytype
				request.session['urindustry'] = industry
				request.session['urplanid'] = planid
				# request.session['uryears'] = years
				# request.session['urmonths'] = months
				request.session['urcontact_person'] = contact_person
				request.session['urcontact_person_designation'] = contact_person_designation
				fs = FileSystemStorage()				
				url1 = fs.save(company_profile.name, company_profile)
				company_profile_url = fs.url(url1)
				request.session['urcompany_profile_url'] = company_profile_url

				otp = (randint(1000, 9999))
				otp = str(otp)
				request.session['val_otp1'] = otp
				subject = 'Freepublik Email Verification'
				msg_plain2 = render_to_string('clientotpemail.html', {})
				msg_html = render_to_string('clientotpemail.html', {'otp': otp})
				email_from = settings.SENDER_EMAIL
				recipient_list = [email]
				send_mail( subject, msg_plain2, email_from, recipient_list,html_message=msg_html )
				return render(request,'uotpvalidation.html',{'total_count':total_count})
		else:
			error = " PASSWORD MISMATCH "
			return render(request, 'login.html',{"error":error,"total_count":total_count})
		# success = "REGISTRATION SUCCESSFUL. YOU CAN LOGIN NOW."		
		# return render(request,'login.html',{"success":success})				
			# print("////")
			# if planid == '0':
			# 	reg_user_obj = reg_users(username=username,email = email,password = pass_1,
			# 		country=country,phone=phone,industry=industry,
			# 		planid=planid)
			# 	reg_user_obj.save()
			# 	# return render(request,'buy_membership.html')
			# else:
			# 	reg_user_obj = reg_users(username=username,email = email,password = pass_1,
			# 		country=country,phone=phone,industry=industry,
			# 		planid=planid)
			# 	reg_user_obj.save()

			# # request.session['uname'] = name
			# success = "REGISTRATION SUCCESSFUL. YOU CAN LOGIN NOW."		
			# return render(request,'login.html',{"success":success})		
		
	else:
		return render(request, 'login.html',{"total_count":total_count})	


def uemailotpverification(request):
	total_count = totalcount()
	if request.session.has_key('uremail') and request.session.has_key('val_otp1'):
		original_otp = request.session['val_otp1']
		enterd_otp = request.POST.get('otp')
		if original_otp == enterd_otp:
			
			email = request.session['uremail']
			pass_1 = request.session['urpass_1']
			country = request.session['urcountry']
			# countrycode = request.session['urcountrycode']
			phone = request.session['urphone']
			company = request.session['urcompany']
			companytype = request.session['urcompanytype']
			industry = request.session['urindustry']
			planid = request.session['urplanid']
			# years = request.session['uryears']
			# months = request.session['urmonths']
			contact_person = request.session['urcontact_person']
			contact_person_designation = request.session['urcontact_person_designation']
			company_profile_url = request.session['urcompany_profile_url']
			date = datetime.now()
			
			reg_user_obj = reg_users(username=email,email = email,password = pass_1,
						country=country,phone=phone,company=company,
						companytype=companytype,industry=industry,planid=planid,contact_person=contact_person,
						contact_person_designation=contact_person_designation,companyprofileurl=company_profile_url)
			reg_user_obj.save()
			success = "REGISTRATION SUCCESSFUL. YOU CAN LOGIN NOW. CHECK YOUR EMAIL FOR CREDENTIALS."
			# print("+++++",username,email,pass_1,country,phone,industry,planid)
			for key in request.session.keys():
				del request.session[key]
			msg_plain = render_to_string('clientwelcomeemail.txt', {'email': email,'password': pass_1})
			msg_html = render_to_string('clientwelcomeemail.html', {'email': email,'password': pass_1})
			send_mail('FreePublik Welcome You',msg_plain,settings.SENDER_EMAIL,[email],html_message=msg_html,)
			msg_plain2 = render_to_string('clientregistereddetailemail.txt', {})
			msg_html2 = render_to_string('clientregistereddetailemail.html', {'id':email,'password': pass_1,'company': company,'date': date,'phone': phone,'email':email,'companytype':companytype})
			send_mail('FreePublik - New Client Registration Details',msg_plain2,settings.SENDER_EMAIL,['hail@freepublik.com','priti@freepublik.com'],html_message=msg_html2)
			# send_mail('FreePublik - New Client Registration Details',msg_plain2,settings.SENDER_EMAIL,['ankit@comportement.in'],html_message=msg_html2)
			return render(request,'login.html',{"success":success,'total_count':total_count})
		else:
			error = "Wrong OTP. Try Again."		
			return render(request,'uotpvalidation.html',{"error":error,'total_count':total_count})
	else:
		return render(request, 'login.html',{'total_count':total_count})




def login_user(request):
	total_count = totalcount()
	if request.method == "POST":
		request.session.get_expire_at_browser_close()
		email = request.POST.get('email')
		password = request.POST.get('password')
		user_details = reg_users.objects.all()
		if user_details:
			for each in user_details:
				if email == each.email and password == each.password:
					request.session['email'] = email
					uname = each.username
					request.session['uname'] = uname
					return redirect('/udashboard/')
			else:
				error = " Sorry! Email Id and Password didn't match, Please try again ! "
				return render(request, 'login.html',{'error':error,"total_count":total_count})
		else:
			error = " Sorry! Email Id and Password is not registered, Please Register first ! "
			return render(request, 'login.html',{'error':error,"total_count":total_count})
	else:
		return render(request, 'login.html',{"total_count":total_count})


def dashboard(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']
		return render(request, 'fdashboard.html', {'name' : fname,"total_count":total_count})
	elif request.session.has_key('uname'):
		uname = request.session['uname']
		return render(request,'udashboard.html', {'name' : uname,"total_count":total_count})
	else:
		return render(request,'index.html',{"total_count":total_count})
	
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def udashboard(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		
		uname = request.session['uname']
		post_list = post.objects.filter(author=uname,freelancer_appointed='',status='open',deletedbyclient=False).order_by("-date")
		if post_list:
			apply_list = post.applied.through.objects.all()
			# 	print(apply_list)
			cursor = connection.cursor()
			cursor.execute("SELECT post_id, count(*) as total FROM publik_post_applied GROUP BY post_id;")
			row = dictfetchall(cursor)
			# final =  int(row['count(*)'])
			# print row
			postlist = []
			for each in row:
				postlist.append(each['post_id'])
				
			# print postlist
			# print final

			cursor1 = connection.cursor()
			cursor1.execute("SELECT post_id, reg_freelancer_id FROM publik_post_applied")
			row1 = dictfetchall(cursor1)
			print row1 


			paginator = Paginator(post_list, 3)
			page = request.GET.get('page')
			try:
				posts = paginator.page(page)
			except PageNotAnInteger:
				posts = paginator.page(1)
			except EmptyPage:
				posts = paginator.page(paginator.num_pages)


			users = reg_users.objects.get(username=uname)
		
			freelancer_details = reg_freelancer.objects.all()		
			return render(request, 'udashboard.html', {'name' : uname,'posts':posts ,'users':users,'row':row,'postlist':postlist,'row1':row1,'freelancer_details':freelancer_details,'total_count':total_count})
		else:
			error = " No Post Yet. Post a Project "
			users = reg_users.objects.get(username=uname)
			return render(request, 'udashboard.html',{"error":error,'users':users,'name' : uname,'total_count':total_count})
	else:
		return render(request,'index.html',{"total_count": total_count})


def profile_view(request,postid,id):
	total_count = totalcount()
	post_selected = ""
	if request.session.has_key('uname'):
		username = request.session['uname']
		# print("*******",id,"*******")
		freelancer_details = reg_freelancer.objects.filter(username=id)
		postdetail = post.objects.get(id=postid)
		if postdetail:
			if postdetail.selected == id or postdetail.freelancer_appointed == id:
				print("inside if bro")
				post_selected = "already selected"
		if freelancer_details:
			for each in freelancer_details:
				username = each.username
				if each.expertise:
					expertise_list = json.loads(each.expertise)
				data = {'username': each.username,'expertise':each.expertise,
				'work1':each.work1,'work2':each.work2,'work3':each.work3,'work4':each.work4,'work5':each.work5,'work6':each.work6,'years':each.years,'months':each.months,'company':each.company,'freelancertype':each.freelancertype,'designation':each.designation,'profile':each.profile,'resume':each.resume}
			print (data)
		return render(request, 'profile_view.html',{"data":data,"postid":postid,"username":username,"expertise_list":expertise_list,'post_selected':post_selected,'total_count':total_count})
	else:
		return render(request, 'login.html',{'total_count':total_count})

def select_candidate(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		username = request.GET.get('user_id')
		postid = request.GET.get('post_id')
		print("=====================",username,postid,"=================")
		post.objects.filter(id=postid).update(selected=username)
		#save the datails in the notification table to generate the notifications to display notifications.

		# freelancer_notifications.objects.filter(candidate=username) 
		data = {"success":"Wait for the Freelancer's Acceptance"}	# save in the post table and check its value there only.	
		return JsonResponse(data)
	else:
		return render(request, 'login.html',{'total_count':total_count})

def project_start(request,postid,username):
	total_count = totalcount()
	if request.session.has_key('fname'):
		post.objects.filter(id=postid).update(freelancer_appointed=username,selected="")
		post_object = post.objects.get(id=postid)
		print(post_object.author)
		user_details = reg_users.objects.get(username=post_object.author)
		client_email = user_details.email
		freelancer_details = reg_freelancer.objects.get(username=username)
		freelancer_email = freelancer_details.email
		print(client_email,freelancer_email)

		# subject = 'Freepublik'
		# message = 'Your project has started. Contact your Freelancer on: %s' %(freelancer_email)
		# email_from = settings.SENDER_EMAIL
		# recipient_list = [client_email]
		# send_mail( subject, message, email_from, recipient_list )
		# message2 = 'Your project has started. Contact the Project Author on: %s' %(client_email)
		# recipient_list2 = [freelancer_email]
		# send_mail( subject, message2, email_from, recipient_list2 )
		message1 = ('FreePublik', 'Your project has started. Contact your Freelancer on: ' + freelancer_email, settings.SENDER_EMAIL, [client_email])
		message2 = ('FreePublik', 'Your project has started. Contact the Project Author on: ' + client_email, settings.SENDER_EMAIL, [freelancer_email])
		send_mass_mail((message1, message2), fail_silently=False)
		return redirect('/fdashboard/')
	else:
		return render(request, 'flogin.html',{'total_count':total_count})



def uprofile(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		uname = request.session['uname']
		client_object = reg_users.objects.get(username=uname)
		# data = {'username':each.username,'email':each.email,'password':each.password,'country':each.country,'phone':each.phone,
				# 'industry':each.industry,'planid':each.planid,'company':each.company,'companytype':each.companytype,'contact_person':each.contact_person,'contact_person_designation':each.contact_person_designation}
		return render(request, 'uprofile.html', {"client_object" :client_object,'total_count':total_count})
	else:
		return render(request,'login.html',{'total_count':total_count})


def uactiveprojects(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		
		uname = request.session['uname']
		post_list = post.objects.filter(author=uname,status='open',deletedbyclient=False).exclude(freelancer_appointed="").order_by("-date")
		if post_list:
			apply_list = post.applied.through.objects.all()
			# 	print(apply_list)
			cursor = connection.cursor()
			cursor.execute("SELECT post_id, count(*) as total FROM publik_post_applied GROUP BY post_id;")
			row = dictfetchall(cursor)
			# final =  int(row['count(*)'])
			# print row
			postlist = []
			for each in row:
				postlist.append(each['post_id'])
				
			# print postlist
			# print final

			cursor1 = connection.cursor()
			cursor1.execute("SELECT post_id, reg_freelancer_id FROM publik_post_applied")
			row1 = dictfetchall(cursor1)
			print row1 


			paginator = Paginator(post_list, 3)
			page = request.GET.get('page')
			try:
				posts = paginator.page(page)
			except PageNotAnInteger:
				posts = paginator.page(1)
			except EmptyPage:
				posts = paginator.page(paginator.num_pages)


			users = reg_users.objects.get(username=uname)
		
			freelancer_details = reg_freelancer.objects.all()		
			return render(request, 'uactiveprojects.html', {'name' : uname,'posts':posts ,'users':users,'row':row,'postlist':postlist,'row1':row1,'freelancer_details':freelancer_details,'total_count':total_count})
		else:
			error = " No Active Project Yet."
			users = reg_users.objects.get(username=uname)
			return render(request, 'uactiveprojects.html',{"error":error,'users':users,'name' : uname,'total_count':total_count})
	else:
		return render(request,'index.html',{'total_count':total_count})

def ucloseprojects(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		
		uname = request.session['uname']
		post_list = post.objects.filter(author=uname,status='close',deletedbyclient=False).exclude(freelancer_appointed="").order_by("-date")
		if post_list:
			apply_list = post.applied.through.objects.all()
			# 	print(apply_list)
			cursor = connection.cursor()
			cursor.execute("SELECT post_id, count(*) as total FROM publik_post_applied GROUP BY post_id;")
			row = dictfetchall(cursor)
			# final =  int(row['count(*)'])
			# print row
			postlist = []
			for each in row:
				postlist.append(each['post_id'])
				
			# print postlist
			# print final

			cursor1 = connection.cursor()
			cursor1.execute("SELECT post_id, reg_freelancer_id FROM publik_post_applied")
			row1 = dictfetchall(cursor1)
			print row1 


			paginator = Paginator(post_list, 3)
			page = request.GET.get('page')
			try:
				posts = paginator.page(page)
			except PageNotAnInteger:
				posts = paginator.page(1)
			except EmptyPage:
				posts = paginator.page(paginator.num_pages)


			users = reg_users.objects.get(username=uname)
		
			freelancer_details = reg_freelancer.objects.all()		
			return render(request, 'ucloseprojects.html', {'name' : uname,'posts':posts ,'users':users,'row':row,'postlist':postlist,'row1':row1,'freelancer_details':freelancer_details,'total_count':total_count})
		else:
			error = " No Active Project Yet."
			users = reg_users.objects.get(username=uname)
			return render(request, 'ucloseprojects.html',{"error":error,'users':users,'name' : uname,'total_count':total_count})
	else:
		return render(request,'index.html',{'total_count':total_count})



def ueditform(request):
	total_count = totalcount()
	if request.session.has_key('uname') and request.method == "POST":
		uname = request.session['uname']		
		password = request.POST.get('password')
		phone = request.POST.get('phone')
		company = request.POST.get('company')
		country = request.POST.get('country')
		category = request.POST.get('category')		
		industry = request.POST.get('industry')
		contact_person = request.POST.get('contact_person')
		contact_person_designation = request.POST.get('contact_person_designation')
		# contact_person_phone = request.POST.get('contact_person_phone')
		# contact_person_email = request.POST.get('contact_person_email')
		# countrycode = phone[0:3]
		
		
		
		reg_users.objects.filter(username=uname).update(password=password, phone=phone,company=company,
			country = country,companytype=category,industry=industry, contact_person = contact_person, 
			contact_person_designation=contact_person_designation)
		print("here")
		return redirect('/uprofile/')
	else:
		return render(request,'uprofile.html',{'total_count':total_count})




def index(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']
		return render(request, 'index.html', {'fname' : fname,'total_count':total_count})
	elif request.session.has_key('uname'):
		uname = request.session['uname']
		return render(request, 'index.html', {'uname' : uname,'total_count':total_count})
	else:	
		return render(request,'index.html',{'total_count':total_count})





def flogin(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return redirect('/fdashboard/')
	else:
		return render(request,'freelancer_login.html',{'total_count':total_count})	



def freepublikanawardsflogin(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return redirect('/fdashboard/')
	elif request.session.has_key('uname'):
		uname = request.session['uname']
		return redirect('/udashboard/')	
	else:
		request.session['fromfreepublikanawards'] = 'True'
		return render(request,'freelancer_login.html',{'total_count':total_count,'fromfreepublikanawards':'True'})


def registration_freelancer(request):
	total_count = totalcount()
	if request.method == 'POST':				
		email = request.POST.get('email')
		pass_1 = request.POST.get('password1')
		pass_2 = request.POST.get('password2')		
		username = request.POST.get('username')
		country = request.POST.get('country')
		# countrycode = request.POST.get('countrycode')
		phone = request.POST.get('phone')
		freelancertype = request.POST.get('freelancertype')
		designation = request.POST.get('designation')
		company = request.POST.get('company')		
		expertise_list = request.POST.getlist('expertise')
		expertise = json.dumps(expertise_list)
		years = request.POST.get('years')
		months = request.POST.get('months')
				
		company_profile1 = request.FILES['company_profile1'] if 'company_profile1' in request.FILES else None
		company_profile2 = request.FILES['company_profile2'] if 'company_profile2' in request.FILES else None
		company_profile3 = request.FILES['company_profile3'] if 'company_profile3' in request.FILES else None
		company_profile4 = request.FILES['company_profile4'] if 'company_profile4' in request.FILES else None
		company_profile5 = request.FILES['company_profile5'] if 'company_profile5' in request.FILES else None
		company_profile6 = request.FILES['company_profile6'] if 'company_profile6' in request.FILES else None

		company_profile_resume = request.FILES['company_profile_resume'] if 'company_profile_resume' in request.FILES else None
		
		planid = request.POST.get('planid')
		
		# print(name,email,pass_1,pass_2,username,country,phone,expertise,yearofexperience,work1,work2,work3,work4,
		# 	work5,resume,lastworkingexp,designation,planid)
		if pass_1 == pass_2:
			freelancer_details = reg_freelancer.objects.all()
			if freelancer_details:
				for each in freelancer_details:
					if each.email == email:
						error = " EMAIL ALREADY EXISTS PLEASE LOGIN "
						return render(request, 'freelancer_login.html',{"error":error,"total_count":total_count})
				else:
					for each in freelancer_details:
						if each.username == username:
							error = " USERNAME IS ALREADY USED "
							return render(request, 'freelancer_login.html',{"error":error,"total_count":total_count})
						if each.phone == phone:
							error = " PHONE NUMBER IS ALREADY USED "
							return render(request, 'freelancer_login.html',{"error":error,"total_count":total_count})
					else:
						fs = FileSystemStorage()
						# print(work1.name,work2.name,work3.name,work4.name,work5.name,resume.name)
						if company_profile1 is not None:
							url1 = fs.save(company_profile1.name, company_profile1)
							company_profile1 = fs.url(url1)
						
						if company_profile2 is not None:
							url2 = fs.save(company_profile2.name, company_profile2)
							company_profile2 = fs.url(url2)
						
						if company_profile3 is not None:
							url3 = fs.save(company_profile3.name, company_profile3)
							company_profile3 = fs.url(url3)
						
						if company_profile4 is not None:
							url4 = fs.save(company_profile4.name, company_profile4)
							company_profile4 = fs.url(url4)
						
						if company_profile5 is not None:
							url5 = fs.save(company_profile5.name, company_profile5)
							company_profile5 = fs.url(url5)
						
						if company_profile6 is not None:
							url6 = fs.save(company_profile6.name, company_profile6)
							company_profile6 = fs.url(url6)
						if company_profile_resume is not None:
							filename = company_profile_resume.name
							filename = filename.split(".")
							file_extension = filename[1]
							
							finalfilename = username+"_resume_"+"."+ file_extension
							# print(finalfilename)
							url7 = fs.save(str(finalfilename), company_profile_resume)
							company_profile_resume = fs.url(url7)
						else:
							company_profile_resume = "not uploaded"
						
						
						request.session['fremail'] = email
						request.session['frpassword'] = pass_1
						request.session['frusername'] = username
						request.session['frcountry'] = country
						# request.session['frcountrycode'] = countrycode
						request.session['frphone'] = phone
						request.session['frfreelancertype'] = freelancertype
						request.session['frdesignation'] = designation
						request.session['frcompany'] = company
						request.session['frexpertise'] = expertise
						request.session['fryears'] = years
						request.session['frmonths'] = months
						request.session['frcompany_profile1'] = company_profile1
						request.session['frcompany_profile2'] = company_profile2
						request.session['frcompany_profile3'] = company_profile3
						request.session['frcompany_profile4'] = company_profile4
						request.session['frcompany_profile5'] = company_profile5
						request.session['frcompany_profile6'] = company_profile6
						request.session['frcompany_profile_resume'] = company_profile_resume						
						request.session['frplanid'] = planid
						# print(email,pass_1,username,country,phone,freelancertype,designation,company,expertise,years,months,company_profile1
						# 	,company_profile2,company_profile3,company_profile4,company_profile5,company_profile6,company_profile_resume,planid)
						otp = (randint(1000, 9999))
						otp = str(otp)
						request.session['val_otp'] = otp
						subject = 'Freepublik Email Verification'
						msg_plain = render_to_string('fotpemail.html', {})
						msg_html = render_to_string('fotpemail.html', {'otp': otp})
						email_from = settings.SENDER_EMAIL
						recipient_list = [email]
						send_mail( subject, msg_plain, email_from, recipient_list,html_message=msg_html )
						return render(request,'fotpvalidation.html')



			else:
				fs = FileSystemStorage()
				# print(work1.name,work2.name,work3.name,work4.name,work5.name,resume.name)
				if company_profile1 is not None:
					url1 = fs.save(company_profile1.name, company_profile1)
					company_profile1 = fs.url(url1)
				
				if company_profile2 is not None:
					url2 = fs.save(company_profile2.name, company_profile2)
					company_profile2 = fs.url(url2)
				
				if company_profile3 is not None:
					url3 = fs.save(company_profile3.name, company_profile3)
					company_profile3 = fs.url(url3)
				
				if company_profile4 is not None:
					url4 = fs.save(company_profile4.name, company_profile4)
					company_profile4 = fs.url(url4)
				
				if company_profile5 is not None:
					url5 = fs.save(company_profile5.name, company_profile5)
					company_profile5 = fs.url(url5)
				
				if company_profile6 is not None:
					url6 = fs.save(company_profile6.name, company_profile6)
					company_profile6 = fs.url(url6)

				if company_profile_resume is not None:
					filename = company_profile_resume.name
					filename = filename.split(".")
					file_extension = filename[1]
					
					finalfilename = username+"_resume_"+"."+ file_extension
					# print(finalfilename)
					url7 = fs.save(str(finalfilename), company_profile_resume)
					company_profile_resume = fs.url(url7)
				else:
					company_profile_resume = "not uploaded"
				
				request.session['fremail'] = email
				request.session['frpassword'] = pass_1
				request.session['frusername'] = username
				request.session['frcountry'] = country
				# request.session['frcountrycode'] = countrycode
				request.session['frphone'] = phone
				request.session['frfreelancertype'] = freelancertype
				request.session['frdesignation'] = designation
				request.session['frcompany'] = company
				request.session['frexpertise'] = expertise
				request.session['fryears'] = years
				request.session['frmonths'] = months
				request.session['frcompany_profile1'] = company_profile1
				request.session['frcompany_profile2'] = company_profile2
				request.session['frcompany_profile3'] = company_profile3
				request.session['frcompany_profile4'] = company_profile4
				request.session['frcompany_profile5'] = company_profile5
				request.session['frcompany_profile6'] = company_profile6
				request.session['frcompany_profile_resume'] = company_profile_resume						
				request.session['frplanid'] = planid
				# print(email,pass_1,username,country,phone,freelancertype,designation,company,expertise,years,months,company_profile1
					# ,company_profile2,company_profile3,company_profile4,company_profile5,company_profile6,company_profile_resume,planid)
				otp = (randint(1000, 9999))
				otp = str(otp)
				request.session['val_otp'] = otp
				subject = 'Freepublik Email Verification'
				msg_plain = render_to_string('fotpemail.html', {})
				msg_html = render_to_string('fotpemail.html', {'otp': otp})
				email_from = settings.SENDER_EMAIL
				recipient_list = [email]
				send_mail( subject, msg_plain, email_from, recipient_list,html_message=msg_html )
				return render(request,'fotpvalidation.html')
		else:
			error = " PASSWORD MISMATCH "
			return render(request, 'freelancer_login.html',{"error":error,"total_count":total_count})
		# success = "REGISTRATION SUCCESSFUL. YOU CAN LOGIN NOW."		
		# return render(request,'freelancer_login.html',{"success":success})				
			# print("////")
			# if planid == '0':
			# 	reg_user_obj = reg_users(username=username,email = email,password = pass_1,
			# 		country=country,phone=phone,industry=industry,
			# 		planid=planid)
			# 	reg_user_obj.save()
			# 	# return render(request,'buy_membership.html')
			# else:
			# 	reg_user_obj = reg_users(username=username,email = email,password = pass_1,
			# 		country=country,phone=phone,industry=industry,
			# 		planid=planid)
			# 	reg_user_obj.save()

			# # request.session['uname'] = name
			# success = "REGISTRATION SUCCESSFUL. YOU CAN LOGIN NOW."		
			# return render(request,'login.html',{"success":success})		
		
	else:
		return render(request, 'freelancer_login.html',{"total_count":total_count})	
	# 	if pass_1 == pass_2:
	# 		print("////")
	# 		fs = FileSystemStorage()
	# 		print(work1.name,work2.name,work3.name,work4.name,work5.name,resume.name)
	# 		url1 = fs.save(work1.name, work1)
	# 		work1url = fs.url(url1)
	# 		url2 = fs.save(work2.name, work2)
	# 		work2url = fs.url(url2)
	# 		url3 = fs.save(work3.name, work3)
	# 		work3url = fs.url(url3)
	# 		url4 = fs.save(work4.name, work4)
	# 		work4url = fs.url(url4)
	# 		url5 = fs.save(work5.name, work5)
	# 		work5url = fs.url(url5)
	# 		url6 = fs.save(resume.name, resume)
	# 		resumeurl = fs.url(url6)
	# 		print(url1,url2,url3,url4,url5,url6)
	# 		if planid == '0':
				

	# 			reg_freelancer_obj = reg_freelancer(name=name,email = email,password = pass_1,username = username, 
	# 				country=country,phone=phone,expertise=expertise,yearofexperience=yearofexperience,
	# 				from_time=from_time,to_time=to_time,work1=work1url,work2=work2url,work3=work3url,work4=work4url
	# 				,work5=work5url,resume=resumeurl,lastworkingexp=lastworkingexp,designation=designation,planid=planid
	# 				)
	# 			reg_freelancer_obj.save()
	# 			# return render(request,'buy_membership.html')
	# 		else:
	# 			# call a function which will done payments and returns true on successful transaction.
	# 			# ex - if membership_buy():
	# 			reg_freelancer_obj = reg_freelancer(name=name,email = email,password = pass_1,username = username, 
	# 				country=country,phone=phone,expertise=expertise,yearofexperience=yearofexperience,
	# 				from_time=from_time,to_time=to_time,work1=work1url,work2=work2url,work3=work3url,work4=work4url
	# 				,work5=work5url,resume=resumeurl,lastworkingexp=lastworkingexp,designation=designation,planid=planid
	# 				)
	# 			reg_freelancer_obj.save()
	# 		# request.session['fname'] = username
	# 		success = "REGISTRATION SUCCESSFUL. YOU CAN LOGIN NOW."	
	# 		return render(request,'freelancer_login.html',{"success":success})
		
	# 	else:
	# 		error = " Password Mismatch "
	# 		return render(request, 'freelancer_login.html',{"error":error})
	# else:
	# 	return render(request, 'freelancer_login.html')	


def login_freelancer(request):
	total_count = totalcount()
	if request.method == "POST":
		request.session.get_expire_at_browser_close()
		email = request.POST.get('email')
		password = request.POST.get('password')
		user_details = reg_freelancer.objects.all()
		if user_details:
			for each in user_details:
				if email == each.email and password == each.password:
					request.session['email'] = email
					fname = each.username
					request.session['fname'] = fname
					if request.session.has_key('fromfreepublikanawards'):
						fromfreepublikanawards = request.session['fromfreepublikanawards']
						# del request.session['fromfreepublikanawards']
						if fromfreepublikanawards:
							freelancer_details = reg_freelancer.objects.get(username=fname)		
							request.session['fremailawards'] = freelancer_details.email
							request.session['frusernameawards'] = freelancer_details.username
							request.session['frcountryawards'] = freelancer_details.country
							request.session['frphoneawards'] = freelancer_details.phone
							request.session['frfreelancertypeawards'] = freelancer_details.freelancertype
							request.session['frcompanyawards'] = freelancer_details.company
							messages.success(request, 'Login Successfull.')
							return render(request, 'awards.html', {'fname' : fname,'backfromfreepublikanawards': fromfreepublikanawards})
					else:	
						return redirect('/fdashboard/')
			else:
				error = " Sorry! Email Id and Password didn't match, Please try again ! "
				return render(request, 'freelancer_login.html',{"error":error,"total_count":total_count})
		else:
			error = " Sorry! Email Id and Password is not registered, Please Register first ! "
			return render(request, 'freelancer_login.html',{"error":error,"total_count":total_count})
	else:
		return render(request, 'freelancer_login.html',{"total_count":total_count})




def logout(request):
	if request.session.has_key('uname'):	
		del request.session['uname']
	elif request.session.has_key('fname'):
		del request.session['fname']
	if request.session.has_key('email'):
		del request.session['email']
	for key in request.session.keys():
		del request.session[key]
	error = " You logged out! "
	return redirect('/')
	# return render(request, 'index.html',{"error":error})



def fdashboard(request, category= None):
	total_count = totalcount()
	if request.session.has_key('fname'):
		applied_list = []
		username = request.session['fname']
		post_list = post.objects.filter(freelancer_appointed=username,selected="",deletedbyfreelancer=False) | post.objects.filter(freelancer_appointed="",selected=username,deletedbyfreelancer=False) | post.objects.filter(freelancer_appointed="",selected="",deletedbyfreelancer=False)
		print("++++++++++++++++++++++++++++++++++++++",post_list,"++++++++++++++++++++++++++++++++++++++++")
		post_list = post_list.order_by("-date")
		if post_list:
			apply_list = post.applied.through.objects.filter(reg_freelancer_id=username)#access the objects of intermediatary table and disable the apply post button based on that
			for each in apply_list:
				applied_list.append(each.post_id) 
			cursor = connection.cursor()
			cursor.execute("SELECT post_id, count(*) as total FROM publik_post_applied GROUP BY post_id;")
			row = dictfetchall(cursor)
			# final =  int(row['count(*)'])
			# print row
			postlist = []
			for each in row:
				postlist.append(each['post_id'])
				
			# print postlist
			# print final

			cursor1 = connection.cursor()
			cursor1.execute("SELECT post_id, reg_freelancer_id FROM publik_post_applied")
			row1 = dictfetchall(cursor1)
			print row1 


			paginator = Paginator(post_list, 3)
			page = request.GET.get('page')
			try:
				posts = paginator.page(page)
			except PageNotAnInteger:
				posts = paginator.page(1)
			except EmptyPage:
				posts = paginator.page(paginator.num_pages)


			freelancer_details = reg_freelancer.objects.get(username=username)
			print freelancer_details.expertise
			if freelancer_details.expertise:
				expertise_list = json.loads(freelancer_details.expertise)
			
					
			return render(request, 'fdashboard.html', {'name' : username,'posts':posts ,'freelancer_details':freelancer_details,'apply_list':applied_list,'expertise_list':expertise_list,'total_count':total_count})
		else:
			error = " No Post Available to Apply "
			freelancer_details = reg_freelancer.objects.get(username=username)
			freelancer_details.expertise
			if freelancer_details.expertise:
				expertise_list = json.loads(freelancer_details.expertise)
			
			
			return render(request, 'fdashboard.html',{"error":error,'name' : username,'freelancer_details':freelancer_details,'expertise_list':expertise_list,'total_count':total_count})
	else:
		return render(request,'index.html',{"total_count":total_count})




def fprofile(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']
		freelancer_details = reg_freelancer.objects.get(username=fname)
		# data = {'username':freelancer_details.username,'email':freelancer_details.email,'password':freelancer_details.password,'country':freelancer_details.country,'phone':freelancer_details.phone,
		# 		'expertise':freelancer_details.expertise,'planid':freelancer_details.planid}
		
		expertise_list = ""
		if freelancer_details.expertise:
			expertise_list = json.loads(freelancer_details.expertise)

		return render(request, 'fprofile.html', {'freelancer_details':freelancer_details,'expertise_list':expertise_list,'total_count':total_count})
	else:
		return render(request,'freelancer_login.html',{"total_count":total_count})

def fmywork(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']
		freelancer_details = reg_freelancer.objects.get(username=fname)
		expertise_list = ""
		if freelancer_details.expertise:
			expertise_list = json.loads(freelancer_details.expertise)
		return render(request, 'fmywork.html',{'freelancer_details':freelancer_details,'expertise_list':expertise_list,'total_count':total_count})
	else:
		return render(request,'freelancer_login.html',{'total_count':total_count})


def fmyworkimageupload(request):
	total_count = totalcount()
	if request.session.has_key('fname') and request.method == "POST":
		fname = request.session['fname']
		work1 = request.FILES['work1'] if 'work1' in request.FILES else None
		work2 = request.FILES['work2'] if 'work2' in request.FILES else None
		work3 = request.FILES['work3'] if 'work3' in request.FILES else None
		work4 = request.FILES['work4'] if 'work4' in request.FILES else None
		work5 = request.FILES['work5'] if 'work5' in request.FILES else None
		work6 = request.FILES['work6'] if 'work6' in request.FILES else None
		new_image_list=[]
		fs = FileSystemStorage()				
		if work1 is not None:
			url1 = fs.save(work1.name, work1)
			work1 = fs.url(url1)
			reg_freelancer.objects.filter(username=fname).update(work1=work1)
		if work2 is not None:
			url2 = fs.save(work2.name, work2)
			work2 = fs.url(url2)
			reg_freelancer.objects.filter(username=fname).update(work2=work2)
		if work3 is not None:
			url3 = fs.save(work3.name, work3)
			work3 = fs.url(url3)
			reg_freelancer.objects.filter(username=fname).update(work3=work3)
		if work4 is not None:
			url4 = fs.save(work4.name, work4)
			work4 = fs.url(url4)
			reg_freelancer.objects.filter(username=fname).update(work4=work4)
		if work5 is not None:
			url5 = fs.save(work5.name, work5)
			work5 = fs.url(url5)
			reg_freelancer.objects.filter(username=fname).update(work5=work5)
		if work6 is not None:
			url6 = fs.save(work6.name, work6)
			work6 = fs.url(url6)
			reg_freelancer.objects.filter(username=fname).update(work6=work6)
		# print(new_image_list)
		# reg_freelancer.objects.update(field=Replace('field', Value('string'), Value('anothervalue')))
		return redirect('/fmywork/')
	else:
		return render(request,'freelancer_login.html',{'total_count':total_count})




def feditform(request):
	total_count = totalcount()
	if request.session.has_key('fname') and request.method == "POST":
		fname = request.session['fname']
		# email = request.POST.get('email')
		password = request.POST.get('password')
		phone = request.POST.get('phone')
		years = request.POST.get('years')
		months = request.POST.get('months')		
		designation = request.POST.get('designation')
		company = request.POST.get('company')
		category = request.POST.get('freelancertype')
		country = request.POST.get('country')
		
		reg_freelancer.objects.filter(username=fname).update(password=password, phone=phone,years=years,months=months,designation=designation,company=company,freelancertype=category,country=country)
		return redirect('/fprofile/')
	else:
		return render(request,'fprofile.html',{'total_count':total_count})


def upload_image(request):
	if request.method == "POST" and request.session.has_key('fname') and request.POST.get('fdashboard') == "1":
		fname = request.session['fname']
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		url = fs.url(name)
		print(url)
		reg_freelancer.objects.filter(username=fname).update(profile=url)
		return redirect('/fdashboard/')
	if request.method == "POST" and request.session.has_key('fname') and request.POST.get('fprofile') == "1":
		fname = request.session['fname']
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		url = fs.url(name)
		print(url)
		reg_freelancer.objects.filter(username=fname).update(profile=url)
		return redirect('/fprofile/')
	if request.method == "POST" and request.session.has_key('fname') and request.POST.get('fmywork') == "1":
		fname = request.session['fname']
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		url = fs.url(name)
		print(url)
		reg_freelancer.objects.filter(username=fname).update(profile=url)
		return redirect('/fmywork/')
	if request.method == "POST" and request.session.has_key('uname') and request.POST.get('udashboard') == "1" :
		uname = request.session['uname']
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		url = fs.url(name)
		print(url)
		reg_users.objects.filter(username=uname).update(profile=url)
		return redirect('/udashboard/')
	if request.method == "POST" and request.session.has_key('uname') and request.POST.get('uprofile') == "1":
		uname = request.session['uname']
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		url = fs.url(name)
		print(url)
		reg_users.objects.filter(username=uname).update(profile=url)
		return redirect('/uprofile/')
	if request.method == "POST" and request.session.has_key('uname') and request.POST.get('post_new') == "1" :
		uname = request.session['uname']
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		url = fs.url(name)
		print(url)
		reg_users.objects.filter(username=uname).update(profile=url)
		return redirect('/post/new/')
	if request.method == "POST" and request.session.has_key('uname') and request.POST.get('uactiveprojects') == "1" :
		uname = request.session['uname']
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		url = fs.url(name)
		print(url)
		reg_users.objects.filter(username=uname).update(profile=url)
		return redirect('/uactiveprojects/')


def post_new(request):
   total_count = totalcount()    
   if request.session.has_key('uname'):
   	uname = request.session['uname']	
   	if request.method == "POST":
   		author = request.session['uname']
   		title = request.POST.get('title')
   		job_type = request.POST.get('job_type')
   		moneytype = request.POST.get('moneytype')
   		budget = request.POST.get('budget')
   		years = request.POST.get('years')
   		months = request.POST.get('months')
   		days = request.POST.get('days')
   		brief = request.POST.get('brief')
   		deliverables = request.POST.get('deliverables')
   		post_obj = post(title = title, job_type = job_type, currency_type = moneytype, budget = budget, years = years, months = months, days = days, brief = brief, deliverables = deliverables, author = author)
   		post_obj.save()
   		data = {'title':title, 'job_type':job_type, 'moneytype': moneytype, 'budget':budget, 'years':years, 'months':months, 'days':days,'brief':brief,'deliverables':deliverables,'author':author}
   		print(data)
   		client_object = reg_users.objects.get(username=uname)
   		# obj = post.objects.latest('id')
   		# print(obj.author)
   		request.session['author'] = author
   		request.session['title'] = title
   		request.session['job_type'] = job_type
   		request.session['moneytype'] = moneytype
   		request.session['budget'] = budget
   		request.session['years'] = years
   		request.session['months'] = months
   		request.session['days'] = days
   		request.session['brief'] = brief
   		request.session['deliverables'] = deliverables

   		return render(request, 'post_new.html', {"data" :data,"client_object" :client_object,'total_count':total_count})
   	else:
   		client_object = reg_users.objects.get(username=uname)
   		return render(request,'post_new.html',{"client_object" :client_object,'total_count':total_count})
   return render(request,'login.html',{'total_count':total_count})

# def savecampaign(request):
# 	if request.session.has_key('uname') and request.method == "POST":
		
# 		return redirect('post_detail', pk=campaign_post_obj.pk)
# 	else:
# 		return render(request,'login.html')
	# 	data = {'campaign_type':campaign_type, 'budget':budget,'brief':brief,'milestones':milestones,
	# 	'team_requirement':team_requirement,'author':fname}
	# 	print(data)
	# 	return render(request, 'createcampaign.html', {"data" :data})
	# else:
	# 	return render(request,'login.html')

def post_edit(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		username = request.session['uname']
		obj = post.objects.filter(author=username).latest('id')
		post.objects.filter(author=username).latest('id').delete()
		client_object = reg_users.objects.get(username=username)
		return render(request, 'post_new.html', {"obj": obj,"client_object" :client_object,'total_count':total_count})
	else:
		return render(request, 'login.html',{'total_count':total_count})


def confirm_post(request):
	total_count = totalcount()
	if request.session.has_key('uname'):
		username = request.session['uname']
		message = 'Project Posted Successfully'
		print(message)
		client_object = reg_users.objects.get(username=username)
		author = request.session['author'] 
   		title = request.session['title']
   		job_type = request.session['job_type']
   		moneytype = request.session['moneytype']
   		budget = request.session['budget']
   		years = request.session['years']
   		months = request.session['months']
   		days = request.session['days']
   		brief = request.session['brief']
   		deliverables = request.session['deliverables']
		del request.session['author']
		del request.session['title']
		del request.session['job_type']
		del request.session['moneytype']
		del request.session['budget']
		del request.session['years']
		del request.session['months']
		del request.session['days']
		del request.session['brief']
		del request.session['deliverables']
		msg_plain2 = render_to_string('post_details.txt', {})
		msg_html2 = render_to_string('post_details.html', {'author':author,'title': title,'job_type': job_type,'moneytype': moneytype,'budget': budget,'years':years,'months':months,'days':days,'brief':brief,'deliverables':deliverables})
		send_mail('FreePublik - New Post Details',msg_plain2,settings.SENDER_EMAIL,['hail@freepublik.com','priti@freepublik.com'],html_message=msg_html2)
		return render(request,'post_new.html',{"message":message,"client_object" :client_object,'total_count':total_count})
	else:
		username = request.session['uname']
		error = 'Some Error Occured. Project Posting Unsuccessful. Please Try Again!!!'
		print (error)
		client_object = reg_users.objects.get(username=username)
		return render(request, 'post_new.html',{"error":error,"client_object" :client_object,'total_count':total_count})


def post_delete(request,id):
	total_count = totalcount()
	if request.session.has_key('uname'):
		username = request.session['uname']
		# post.objects.filter(author=username).get(pk=id).delete()
		# post.applied.through.objects.filter(post_id=id).delete()
		post.objects.filter(author=username,pk=id).update(deletedbyclient = True)
		q1 = post.objects.filter(author=username,pk=id)
		for i in q1:
			if i.deletedbyfreelancer == True:
				post.applied.through.objects.filter(post_id=id).delete()
				post.objects.filter(author=username).get(pk=id).delete()
		return redirect('/ucloseprojects/')
	elif (request.session.has_key('fname')):
		username = request.session['fname']
		post.objects.filter(freelancer_appointed=username,pk=id).update(deletedbyfreelancer = True)
		q2 = post.objects.filter(author=username,pk=id)
		for i in q2:
			if i.deletedbyclient == True:
				post.applied.through.objects.filter(post_id=id).delete()
				post.objects.filter(freelancer_appointed=username).get(pk=id).delete()
		return redirect('/fdashboard/')
	else:
		return render(request, 'login.html',{'total_count':total_count})

def post_close(request,id):
	total_count = totalcount()
	try:
		if request.session.has_key('uname'):
			username = request.session['uname']
			post_id = id
			print(id)
			option1 = request.POST.get('group1')
			option2 = request.POST.get('group2')
			option3 = request.POST.get('group3')
			option4 = request.POST.get('group4')
			option5 = request.POST.get('group5')
			option6 = request.POST.get('group6')
			
			post_detail = post.objects.get(pk=id)
			freelancer_username = post_detail.freelancer_appointed
			freelancer_details = reg_freelancer.objects.get(username=freelancer_username)
			if freelancer_details.rating:
				prev_rating = float(freelancer_details.rating)
			else:
				prev_rating = 0
			
			totalrating = float(option1) + float(option2) + float(option3) + float(option4) + float(option5) + float(option6)
			print(totalrating)

			number_of_ratings = float(6)

			averagerating = float(totalrating/number_of_ratings)

			if prev_rating == 0: 
				overall_rating = float(averagerating)
			else:
				overall_rating = float((averagerating + prev_rating)/2)
			print(overall_rating)
			reg_freelancer.objects.filter(username=freelancer_username).update(rating = overall_rating)
			
			# post.applied.through.objects.filter(post_id=id).delete()
			message1 = ('FreePublik', 'You have closed Your project on Freepublik. In case of any support, contact us on : ' + 'hail@freepublik.com', settings.SENDER_EMAIL, [post_detail.author])
			message2 = ('FreePublik', 'One of your projects you were working on has been closed by the Client.In case of any support, contact us on : ' + 'hail@freepublik.com', settings.SENDER_EMAIL, [freelancer_details.email])
			send_mass_mail((message1, message2), fail_silently=False)
			post.objects.filter(id=id).update(status = "close")
			return redirect('/uactiveprojects/')
		else:
			return render(request, 'login.html',{'total_count':total_count})
	except:
		messages.error(request, 'Please check your internet & Try again.')
		return redirect('/uactiveprojects/')
#moved to dashboard

# def post_apply(request):
# 	if request.session.has_key('fname'):
# 		applied_list = []
# 		username = request.session['fname']
# 		apply_list = post.applied.through.objects.filter(reg_freelancer_id=username)#access the objects of intermediatary table and disable the apply post button based on that
# 		for each in apply_list:
# 			applied_list.append(each.post_id) 
# 		post_list = post.objects.order_by("-date")
# 		paginator = Paginator(post_list, 3)
# 		page = request.GET.get('page')
# 		try:
# 			posts = paginator.page(page)
# 		except PageNotAnInteger:
# 			posts = paginator.page(1)
# 		except EmptyPage:
# 			posts = paginator.page(paginator.num_pages)
# 		return render(request, 'post_apply.html', {'posts':posts,'username':username, 'apply_list':applied_list})
# 	else:
# 		return render(request,'freelancer_login.html')


def show_interest(request,id):
	# mylist =[]
	total_count = totalcount()
	try:
		if request.session.has_key('fname'):
			username = request.session['fname']
			# myJsonList1 = post.objects.get(id=id)					#add if condition here in case the post doesn't exist(deleted while applying).
			# number_of_applicant = myJsonList1.number_of_applicant
			# mylist = json.dumps(myJsonList1.apply_list)
			# myPythonList = json.loads(mylist)
			# print(myPythonList)
			# if myPythonList:
			# 	myPythonList.append(username)
			# 	number_of_applicant =  number_of_applicant + 1
			# else:
			# 	myPythonList = [username]
			# 	number_of_applicant = number_of_applicant + 1
			# myJsonList = json.dumps(myPythonList)
			# post.objects.filter(id=id).update(apply_list=myJsonList,number_of_applicant=number_of_applicant)

			# # myJsonList2 = post.objects.get(id=id)
			# # names = json.loads(myJsonList2.apply_list)
			# # 		for i in names:
			# # 			count = count + 1
			# # 	print count
			
			# # myJsonList1 = post.objects.get(id=id)
			# # jsonDec = json.decoder.JSONDecoder()
			# # myPythonList = jsonDec.decode(myJsonList1.apply_list)
			# # print(myPythonList)
			posts = post.objects.get(pk=id)
			# posts.applied.add(username)
			post_detail = posts.applied.all()
			print(post_detail)

			#Email Succeessfull!!!
			user_details = reg_users.objects.get(username=posts.author)
			freelancer_details = reg_freelancer.objects.get(username=username)
			client_email = user_details.email
			# freelancer_email = freelancer_details.email
			freelancer_username = freelancer_details.username
			print(client_email,freelancer_username,"///////////////////////////////////////++++++++++")

			# message1 = ('FreePublik', 'Your project has started. Contact your Freelancer on: ' + freelancer_email, settings.SENDER_EMAIL, [client_email])
			
			subject = 'Freepublik - Applicants'
			message = '%s has applied for your post. You can review on: freepublik.com' %(freelancer_username)
			email_from = settings.SENDER_EMAIL
			recipient_list = [client_email]
			send_mail( subject, message, email_from, recipient_list )

			posts.applied.add(username)

			return redirect('/fdashboard/')
		else:
			return render(request, 'freelancer_login.html',{'total_count':total_count})
	except:
		messages.error(request, 'Please check your internet & Try again.')
		return redirect('/fdashboard/')



def freelancer_notifications(request):
	if request.session.has_key('fname'):
		name = request.session['fname']
		return render(request, 'freelancer_notifications.html')


def aboutus(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return render(request, 'aboutus.html', {'fname' : name,'total_count':total_count})
	elif request.session.has_key('uname'):
		name = request.session['uname']
		return render(request, 'aboutus.html', {'uname' : name,'total_count':total_count})
	else:
		return render(request,'aboutus.html',{'total_count':total_count})


def sitemap(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return render(request, 'sitemap.html', {'fname' : name,'total_count':total_count})
	elif request.session.has_key('uname'):
		name = request.session['uname']
		return render(request, 'sitemap.html', {'uname' : name,'total_count':total_count})
	else:
		return render(request,'sitemap.html',{'total_count':total_count})


def contactus(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return render(request, 'contactus.html', {'fname' : name,'total_count':total_count})
	elif request.session.has_key('uname'):
		name = request.session['uname']
		return render(request, 'contactus.html', {'uname' : name,'total_count':total_count})
	else:
		return render(request,'contactus.html',{'total_count':total_count})


def howitworks(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return render(request, 'howitworks.html', {"fname" : name, 'total_count':total_count})
	elif request.session.has_key('uname'):
		name = request.session['uname']
		return render(request, 'howitworks.html', {"uname" : name, 'total_count':total_count})
	else:
		return render(request,'howitworks.html',{'total_count':total_count})

def privacy(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return render(request, 'privacy.html', {"fname" : name,'total_count':total_count})
	elif request.session.has_key('uname'):
		name = request.session['uname']
		return render(request, 'privacy.html', {"uname" : name,'total_count':total_count})
	else:
		return render(request,'privacy.html', {'total_count':total_count})


def category(request):
	total_count = totalcount()
	if request.session.has_key('fname'):
		name = request.session['fname']
		return render(request, 'category.html', {"fname" : name,'total_count':total_count})
	elif request.session.has_key('uname'):
		name = request.session['uname']
		return render(request, 'category.html', {"uname" : name, 'total_count':total_count})
	else:
		return render(request,'category.html',{'total_count':total_count})



def femailotpverification(request):
	total_count = totalcount()
	if request.session.has_key('fremail') and request.session.has_key('val_otp'):
		original_otp = request.session['val_otp']
		enterd_otp = request.POST.get('otp')
		if original_otp == enterd_otp:
			email = request.session['fremail']
			pass_1 = request.session['frpassword']
			username = request.session['frusername']
			country = request.session['frcountry']
			# countrycode = request.session['frcountrycode']
			phone = request.session['frphone']
			freelancertype = request.session['frfreelancertype']
			designation = request.session['frdesignation']
			company = request.session['frcompany']
			expertise = request.session['frexpertise']
			years = request.session['fryears']
			months = request.session['frmonths']
			company_profile1 = request.session['frcompany_profile1']
			company_profile2 = request.session['frcompany_profile2']
			company_profile3 = request.session['frcompany_profile3']
			company_profile4 = request.session['frcompany_profile4']
			company_profile5 = request.session['frcompany_profile5']
			company_profile6 = request.session['frcompany_profile6']
			company_profile_resume = request.session['frcompany_profile_resume']						
			planid = request.session['frplanid']
			date = datetime.now()
			fromfreepublikanawards = ''
			if request.session.has_key('fromfreepublikanawards'):
				fromfreepublikanawards = request.session['fromfreepublikanawards']
				
			reg_freelancer_obj = reg_freelancer(email = email,password = pass_1,username = username,country=country,
				phone=phone,freelancertype=freelancertype,designation=designation,company=company,expertise=expertise,years=years,months=months,
				work1=company_profile1,work2=company_profile2,work3=company_profile3,work4=company_profile4,work5=company_profile5,
				work6=company_profile6,resume=company_profile_resume,planid=planid)
			reg_freelancer_obj.save()
			success = "REGISTRATION SUCCESSFUL. YOU CAN LOGIN NOW. CHECK YOUR EMAIL FOR CREDENTIALS."
			# print("+++++",name,email,pass_1,username,country,phone,expertise,yearofexperience,work1,work2,work3,work4,work5,resumeurl,lastworkingexp,designation,planid)
			for key in request.session.keys():
				del request.session[key]
			# msg_plain = render_to_string('email.txt', {'email': email,'password': pass_1})
			# msg_html = render_to_string('email.html', {'email': email,'password': pass_1})
			# send_mail('FreePublik Welcome You',msg_plain,settings.SENDER_EMAIL,[email],html_message=msg_html,)
			# send_mail('FreePublik - New Freelancer Registration Details',msg_plain,settings.SENDER_EMAIL,['hail@freepublik.com'],html_message=msg_html,)
			msg_plain = render_to_string('freelancerwelcomeemail.txt', {'email': email,'password': pass_1})
			msg_html = render_to_string('freelancerwelcomeemail.html', {'email': email,'password': pass_1})
			send_mail('FreePublik Welcome You',msg_plain,settings.SENDER_EMAIL,[email],html_message=msg_html,)
			msg_plain2 = render_to_string('freelancerregistereddetailemail.txt', {})
			msg_html2 = render_to_string('freelancerregistereddetailemail.html', {'id':email,'password': pass_1,'company': company,'date': date,'phone': phone,'email':email,'freelancertype':freelancertype})
			send_mail('FreePublik - New Freelancer Registration Details',msg_plain2,settings.SENDER_EMAIL,['hail@freepublik.com','priti@freepublik.com'],html_message=msg_html2)
			
			if fromfreepublikanawards:
				request.session['fname'] = username
				request.session['fremailawards'] = email
				request.session['frusernameawards'] = username
				request.session['frcountryawards'] = country
				request.session['frphoneawards'] = phone
				request.session['frfreelancertypeawards'] = freelancertype
				request.session['frcompanyawards'] = company
				# return redirect('/awards_about/')
				messages.success(request, 'Login Successfull.')
				return render(request, 'awards.html', {'fname' : username,'backfromfreepublikanawards': fromfreepublikanawards})
			else:		
				return render(request,'freelancer_login.html',{"success":success,'total_count':total_count})
		else:
			error = "Wrong OTP. Try Again."		
			return render(request,'fotpvalidation.html',{"error":error,'total_count':total_count})
	else:
		return render(request, 'freelancer_login.html',{'total_count':total_count})

#call this function to copy all resumes from media folder to resume folder
import os
import shutil
def welcomemail(request):
	# msg_plain = render_to_string('email.txt', {'email': "ankit",'password': "ankitp"})
	# msg_html = render_to_string('email.html', {'email': "ankit",'password': "ankitp"})
	files = os.listdir('./media/')
	user_details = reg_freelancer.objects.all()
	for i in files:
		for each in user_details:
			if i in each.resume:
				print("file names",i)
				shutil.copy(os.path.join('./media/',i), './resume/')
				break
	# send_mail(
	#     'TEST email title',
	#     msg_plain,
	#     settings.SENDER_EMAIL,
	#     ['ankit@comportement.in'],
	#     html_message=msg_html,
	# )
	# post.objects.filter(id=50).update(freelancer_appointed="",selected="")
	return render(request,'index.html')


def cforgotpassword(request):
	total_count = totalcount()
	return render(request,'cforgotpassword.html',{'total_count':total_count})

def cforgotemailverification(request):
	total_count = totalcount()
	cemail = request.POST.get('cemail')
	email_list = reg_users.objects.all().values_list('email', flat=True)
	print(email_list)
	if cemail in email_list:
		request.session['cemail'] = cemail
		otp = (randint(1000, 9999))
		otp = str(otp)
		request.session['reset_otp'] = otp
		subject = 'Freepublik Forgot Password Request'
		message = 'Enter the following OTP on website : %s' %(otp)
		email_from = settings.SENDER_EMAIL
		recipient_list = [cemail]
		send_mail( subject, message, email_from, recipient_list )
		return render(request,'cotp.html',{'total_count':total_count})
	else:
		error = "Email ID doesn't exist. Please Register."		
		return render(request,'cforgotpassword.html',{"error":error,'total_count':total_count})

def cotp(request):
	total_count = totalcount()
	if request.session.has_key('cemail') and request.session.has_key('reset_otp'):
		original_otp = request.session['reset_otp']
		enterd_otp = request.POST.get('otp')
		if original_otp == enterd_otp:
			cemail = request.session['cemail']
			return render(request,'cnewpassword.html',{'total_count':total_count})
		else:
			error = "Wrong OTP. Try Again."		
			return render(request,'cotp.html',{"error":error,'total_count':total_count})

	else:
		return render(request,'login.html',{'total_count':total_count})


def cnewpassword(request):
	total_count = totalcount()
	if request.session.has_key('cemail'):
		cemail = request.session['cemail']
		password1 = request.POST.get('password1')
		password2 = request.POST.get('password2')
		if password1 == password2:
			reg_users.objects.filter(email=cemail).update(password=password1)
			success = "Password Changed. You can Login Now."
			del request.session['cemail']
			del request.session['reset_otp']
			subject = 'Freepublik Account Password Updated'
			message = 'Hi! Your Acoount Password has been Updated. Please use the new credentials given below :\n Email: %s \n Password : %s' %(cemail,password1)
			email_from = settings.SENDER_EMAIL
			recipient_list = [cemail]
			send_mail( subject, message, email_from, recipient_list )
			return render(request,'login.html',{"success":success,'total_count':total_count})
		else:
			error = "Password incorrect. Try again"
			return render(request,'cnewpassword.html',{"error":error,'total_count':total_count})
	else:
		return render(request,'login.html',{'total_count':total_count})


def fforgotpassword(request):
	total_count = totalcount()
	return render(request,'fforgotpassword.html',{'total_count':total_count})

def fforgotemailverification(request):
	total_count = totalcount()
	femail = request.POST.get('femail')
	email_list = reg_freelancer.objects.all().values_list('email', flat=True)
	print(email_list)
	if femail in email_list:
		request.session['femail'] = femail
		otp = (randint(1000, 9999))
		otp = str(otp)
		request.session['reset_otp2'] = otp
		subject = 'Freepublik Forgot Password Request'
		message = 'Enter the following OTP on website : %s' %(otp)
		email_from = settings.SENDER_EMAIL
		recipient_list = [femail]
		send_mail( subject, message, email_from, recipient_list )
		return render(request,'fotp.html',{'total_count':total_count})
	else:
		error = "Email ID doesn't exist. Please Register."		
		return render(request,'fforgotpassword.html',{"error":error,'total_count':total_count})

def fotp(request):
	total_count = totalcount()
	if request.session.has_key('femail') and request.session.has_key('reset_otp2'):
		original_otp = request.session['reset_otp2']
		enterd_otp = request.POST.get('otp')
		if original_otp == enterd_otp:
			femail = request.session['femail']			
			return render(request,'fnewpassword.html',{'total_count':total_count})

		else:
			error = "Wrong OTP. Try Again."		
			return render(request,'fotp.html',{"error":error,'total_count':total_count})

	else:
		return render(request,'freelancer_login.html',{'total_count':total_count})


def fnewpassword(request):
	total_count = totalcount()
	if request.session.has_key('femail'):
		femail = request.session['femail']
		password1 = request.POST.get('password1')
		password2 = request.POST.get('password2')
		if password1 == password2:
			reg_freelancer.objects.filter(email=femail).update(password=password1)
			success = "Password Changed. You can Login Now."
			del request.session['femail']
			del request.session['reset_otp2']
			subject = 'Freepublik Account Password Updated'
			message = 'Hi! Your Acoount Password has been Updated. Please use the new credentials given below :\n Email: %s \n Password : %s \n Login : ' %(femail,password1)
			email_from = settings.SENDER_EMAIL
			recipient_list = [femail]
			send_mail( subject, message, email_from, recipient_list )
			return render(request,'freelancer_login.html',{"success":success,'total_count':total_count})
		else:
			error = "Password incorrect. Try again"
			return render(request,'fnewpassword.html',{"error":error,'total_count':total_count})
	else:
		return render(request,'freelancer_login.html',{'total_count':total_count})



def awards_about(request):
	# total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']
		freelancer_details = reg_freelancer.objects.get(username=fname)		
		request.session['fremailawards'] = freelancer_details.email
		request.session['frusernameawards'] = freelancer_details.username
		request.session['frcountryawards'] = freelancer_details.country
		request.session['frphoneawards'] = freelancer_details.phone
		request.session['frfreelancertypeawards'] = freelancer_details.freelancertype
		request.session['frcompanyawards'] = freelancer_details.company
		if request.session.has_key('submitagain'):
			submitagain = request.session['submitagain']
			del request.session['submitagain']
			return render(request, 'awards.html', {'fname' : fname,'submitagain' : submitagain})
		else:
			return render(request, 'awards.html', {'fname' : fname})
	elif request.session.has_key('uname'):
		uname = request.session['uname']
		return render(request, 'awards.html', {'uname' : uname})
	else:	
		return render(request,'awards.html')


def awards_about_form(request):
	# total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']
		freelancer_details = reg_freelancer.objects.get(username=fname)		
		request.session['fremailawards'] = freelancer_details.email
		request.session['frusernameawards'] = freelancer_details.username
		request.session['frcountryawards'] = freelancer_details.country
		request.session['frphoneawards'] = freelancer_details.phone
		request.session['frfreelancertypeawards'] = freelancer_details.freelancertype
		request.session['frcompanyawards'] = freelancer_details.company
		return render(request, 'awards.html', {'fname' : fname,'fromhome':'true'})
	elif request.session.has_key('uname'):
		uname = request.session['uname']
		return render(request, 'awards.html', {'uname' : uname,'fromhome':'true'})
	else:	
		return render(request,'awards.html', {'fromhome':'true'})

def awards_live(request):
	# total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']
		freelancer_details = reg_freelancer.objects.get(username=fname)		
		request.session['fremailawards'] = freelancer_details.email
		request.session['frusernameawards'] = freelancer_details.username
		request.session['frcountryawards'] = freelancer_details.country
		request.session['frphoneawards'] = freelancer_details.phone
		request.session['frfreelancertypeawards'] = freelancer_details.freelancertype
		request.session['frcompanyawards'] = freelancer_details.company
		return render(request, 'awards.html', {'fname' : fname,'fromhome':'true'})
	elif request.session.has_key('uname'):
		uname = request.session['uname']
		return render(request, 'awards.html', {'uname' : uname,'fromhome':'true'})
	else:	
		return render(request,'awards.html', {'fromhometolive':'true'})


def entryformsubmit(request):
	# total_count = totalcount()
	if request.session.has_key('fname'):
		fname = request.session['fname']

		freelanceremail = request.session['fremailawards']
		freelancerusername = request.session['frusernameawards']
		freelancercountry = request.session['frcountryawards']
		freelancerphone = request.session['frphoneawards']
		freelancertype = request.session['frfreelancertypeawards']
		freelancercompany = request.session['frcompanyawards']

		entryformusertype = request.POST.get('freelancertype')
		maincategories = request.POST.get('maincategories')
		subcategories = request.POST.get('subcategories')
		creativelink = request.POST.get('creativelink')
		entryformfile = request.FILES['entryformfile']
		print("here is value <<<>>>>><><><><><><><><><>")
		print(freelanceremail,freelancerusername,freelancercountry,freelancerphone,
			freelancertype,freelancercompany,entryformusertype,maincategories,subcategories,creativelink)
		print("End here is value--------------------------")

		subject = 'Freepublikan - Entry Form Details'
		# message = 'Hi! New Entry Form details are given below :\n Email: %s \n Username : %s \n ' %(freelanceremail,freelancerusername)
		email_from = settings.SENDER_EMAIL
		recipient_list = ['freepublik1@gmail.com']
		msg_html = render_to_string('freepublikanentryformdetail.html', {'freelanceremail': freelanceremail,'freelancerusername': freelancerusername,
			'freelancercountry':freelancercountry,'freelancerphone':freelancerphone,'freelancertype':freelancertype,'freelancercompany':freelancercompany,
			'entryformusertype':entryformusertype,'maincategories':maincategories,'subcategories':subcategories,'creativelink':creativelink})
		mail = EmailMessage(subject, msg_html, settings.SENDER_EMAIL, recipient_list)
		mail.content_subtype = "html"
		mail.attach(entryformfile.name, entryformfile.read(), entryformfile.content_type)
		mail.send()

		subject1 = 'Freepublikan Awards - Successful Submission'
		# message = 'Hi! New Entry Form details are given below :\n Email: %s \n Username : %s \n ' %(freelanceremail,freelancerusername)
		
		recipient_list1 = [freelanceremail]
		msg_html1 = render_to_string('freepublikanentryformdetailconfirmation.html', {'freelanceremail': freelanceremail,'freelancerusername': freelancerusername,
			'freelancercountry':freelancercountry,'freelancerphone':freelancerphone,'freelancertype':freelancertype,
			'entryformusertype':entryformusertype,'maincategories':maincategories,'subcategories':subcategories,'creativelink':creativelink})
		mail = EmailMessage(subject1, msg_html1, settings.SENDER_EMAIL, recipient_list1)
		mail.content_subtype = "html"
		# mail.attach(entryformfile.name, entryformfile.read(), entryformfile.content_type)
		mail.send()


		request.session['submitagain'] = 'True'
		# return render(request,'awards.html', {'fname' : fname})
		return redirect('/awards_about/')
	else:	
		return render(request,'freelancer_login.html',{'total_count': 0 })



def awards_about_jury1(request):
	return render(request, 'awards.html', {'goto1' : 'jury1'})

def awards_about_jury2(request):
	return render(request, 'awards.html', {'goto2' : 'jury2'})	

def awards_about_jury3(request):
	return render(request, 'awards.html', {'goto3' : 'jury3'})

def awards_about_jury4(request):
	return render(request, 'awards.html', {'goto4' : 'jury4'})

def awards_about_jury5(request):
	return render(request, 'awards.html', {'goto5' : 'jury5'})

def awards_about_jury6(request):
	return render(request, 'awards.html', {'goto6' : 'jury6'})

def awards_about_jury7(request):
	return render(request, 'awards.html', {'goto7' : 'jury7'})

def awards_about_jury8(request):
	return render(request, 'awards.html', {'goto8' : 'jury8'})

def awards_about_jury9(request):
	return render(request, 'awards.html', {'goto9' : 'jury9'})

def awards_about_jury10(request):
	return render(request, 'awards.html', {'goto10' : 'jury10'})

def awards_about_jury11(request):
	return render(request, 'awards.html', {'goto11' : 'jury11'})

def awards_about_jury12(request):
	return render(request, 'awards.html', {'goto12' : 'jury12'})

def awards_about_jury13(request):
	return render(request, 'awards.html', {'goto13' : 'jury13'})

def awards_about_jury14(request):
	return render(request, 'awards.html', {'goto14' : 'jury14'})

def google_ads(request):
	return render(request, 'ads.txt')