# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import Group,User


# Register your models here.

from .models import reg_users, reg_freelancer,post
class reg_users_admin(admin.ModelAdmin):
	list_display = ('email','country','phone','industry','planid')
	search_fields = ('email','country','phone','industry')
	list_filter = ['industry']
	exclude = ['countrycode']

class reg_freelancer_admin(admin.ModelAdmin):
	list_display = ('username','email','phone','expertise','planid','freelancertype')
	search_fields = ('username','email','phone','expertise','planid','freelancertype')
	list_filter = ['freelancertype','expertise']
	exclude = ['countrycode']

class post_admin(admin.ModelAdmin):
	list_display = ('job_type','brief','budget','years','days','deliverables','author','date')
	search_fields = ('job_type','brief','budget','years','days','deliverables','author')
	list_filter = ('date','author')
	exclude = ['freelancer_appointed','selected','applied','deletedbyclient','deletedbyfreelancer']

admin.site.register(reg_users,reg_users_admin)
admin.site.register(reg_freelancer,reg_freelancer_admin)
admin.site.register(post,post_admin)
admin.site.unregister(Group)
admin.site.unregister(User)
