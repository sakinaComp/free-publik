"""company_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from publik.views import *
from django.conf import settings
from django.conf.urls.static import static
from django.http import HttpResponseRedirect as rdrct


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', login, name='login'), 
    url(r'^userreg/$', registration_user, name='userreg'),
    url(r'^userlogin/$', login_user, name='userlogin'),
   
    url(r'^$', index, name='index'),
   
    url(r'^flogin/$', flogin, name='flogin'),
    url(r'^freelancereg/$', registration_freelancer, name='freereg'),
    url(r'^freelancerlogin/$', login_freelancer, name='freelogin'),
   
    url(r'^logout/$', logout, name='logout'),
   	url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^fdashboard/$', fdashboard, name='fdashboard'),
    url(r'^fprofile/$', fprofile, name='fprofile'),
    url(r'^fmywork/$', fmywork, name='fmywork'),
    url(r'^fmyworkimageupload/$', fmyworkimageupload, name='fmyworkimageupload'),

    #url(r'^editprofile/$', editprofile, name='editprofile'),
    url(r'^feditform/$', feditform, name='feditform'),
    url(r'^upload_image/$', upload_image, name='upload_image'),
   
    url(r'^udashboard/$', udashboard, name='udashboard'),
 	url(r'^uprofile/$', uprofile, name='uprofile'),
    url(r'^ueditform/$', ueditform, name='ueditform'),
    url(r'^post/new/$', post_new, name='post_new'),
    url(r'^post/edit/$', post_edit, name='post_edit'),
    url(r'^post/confirm/$', confirm_post, name='confirm_post'),
    url(r'^uactiveprojects/$', uactiveprojects, name='uactiveprojects'),
    url(r'^ucloseprojects/$', ucloseprojects, name='ucloseprojects'),
    url(r'^/post/delete/(?P<id>\d+)/$', post_delete, name='post_delete'),
    url(r'^/post/close/(?P<id>\d+)/$', post_close, name='post_close'),
    # url(r'^post/view/$', post_apply, name='post_apply'), 							#moved to dashboard
    url(r'^/post/interest/(?P<id>\d+)/$', show_interest, name='show_interest'),

    # url(r'^post/<int:pk>/$', post_detail, name='post_detail'),
    url(r'^/profile/view/(?P<postid>\d+)/(?P<id>\w+)/$', profile_view, name='profile_view'),
	url(r'^select_candidate/$', select_candidate, name='select_candidate'),   
	url(r'^/project_start/(?P<postid>\d+)/(?P<username>\w+)/$', project_start, name='project_start'), 
	url(r'^fdashboard/freelancer_notifications/$', freelancer_notifications, name='freelancer_notifications'),

    url(r'^sitemap/$', sitemap, name='sitemap'),
    url(r'^aboutus/$', aboutus, name='aboutus'),
    url(r'^contactus/$', contactus, name='contactus'),
    url(r'^howitworks/$', howitworks, name='howitworks'),
    url(r'^privacy/$', privacy, name='privacy'),
    url(r'^category/$', category, name='category'),
    url(r'^femailotpverification/$', femailotpverification, name='femailotpverification'),
    url(r'^uemailotpverification/$', uemailotpverification, name='uemailotpverification'),
    url(r'^welcomemail/$', welcomemail, name='welcomemail'),
    url(r'^cforgotpassword/$', cforgotpassword, name='cforgotpassword'),
    url(r'^cforgotemailverification/$', cforgotemailverification, name='cforgotemailverification'),
    url(r'^cotp/$', cotp, name='cotp'),
    url(r'^cnewpassword/$', cnewpassword, name='cnewpassword'),

    url(r'^fforgotpassword/$', fforgotpassword, name='fforgotpassword'),
    url(r'^fforgotemailverification/$', fforgotemailverification, name='fforgotemailverification'),
    url(r'^fotp/$', fotp, name='fotp'),
    url(r'^fnewpassword/$', fnewpassword, name='fnewpassword'),
    url(r'^awards_about/$', awards_about, name='awards_about'),
    url(r'^awards_about_finalist/$', awards_about_form, name='awards_about_form'),
    url(r'^awards_live/$', awards_live, name='awards_live'),
    url(r'^freepublikanawardsflogin/$', freepublikanawardsflogin, name='freepublikanawardsflogin'),
    url(r'^entryformsubmit/$', entryformsubmit, name='entryformsubmit'),
    url(r'^awards_about_jury1/$', awards_about_jury1, name='awards_about_jury1'),
    url(r'^awards_about_jury2/$', awards_about_jury2, name='awards_about_jury2'),
    url(r'^awards_about_jury3/$', awards_about_jury3, name='awards_about_jury3'),
    url(r'^awards_about_jury4/$', awards_about_jury4, name='awards_about_jury4'),
    url(r'^awards_about_jury5/$', awards_about_jury5, name='awards_about_jury5'),
    url(r'^awards_about_jury6/$', awards_about_jury6, name='awards_about_jury6'),
    url(r'^awards_about_jury7/$', awards_about_jury7, name='awards_about_jury7'),
    url(r'^awards_about_jury8/$', awards_about_jury8, name='awards_about_jury8'),
    url(r'^awards_about_jury9/$', awards_about_jury9, name='awards_about_jury9'),
    url(r'^awards_about_jury10/$', awards_about_jury10, name='awards_about_jury10'),
    url(r'^awards_about_jury11/$', awards_about_jury11, name='awards_about_jury11'),
    url(r'^awards_about_jury12/$', awards_about_jury12, name='awards_about_jury12'),
    url(r'^awards_about_jury13/$', awards_about_jury13, name='awards_about_jury13'),
    url(r'^awards_about_jury14/$', awards_about_jury14, name='awards_about_jury14'),
    url(r'^ads.txt$', google_ads, name='google_ads'),
    url(r'^ads.txt/$', lambda x: rdrct('/ads.txt')),
    


]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


admin.site.site_header = 'FreePublik Administration'
